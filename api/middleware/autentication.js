const jwt = require('jsonwebtoken');

let auth = (req, res, next) => {

    // jwt token generate, res token if procces dont fail
    let token = req.get("Authorization");

// generate token with secret key
    jwt.verify(token, process.env.SECRET, (err, usuario)=>{

        if(err){
            return res.status(500).json({
                ok: false,
                men: "Token no valido"
            });
        }

        req.usuario = usuario.data;

        next();
    });
}

let requireLoginAnd2FA = (req, res)  => {
    
    //  if thowfactor is verify res true
    if ( req.session.authy == true) {
        console.log("RL2FA:  User logged and 2FA");
        return res.status(200).json({
            ok: true
        });
    } else{
        console.log("RL2FA:  User logged in but no 2FA");
        return res.status(500).json({
            ok: false,
            err: "no 2 factor"
        });
    } 
}


module.exports = {
    auth,
    requireLoginAnd2FA
}