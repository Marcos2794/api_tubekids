const User = require("../models/userModels");
const bcrypt = require("bcrypt");
var crypto = require("crypto");
const jwt = require("jsonwebtoken");
var moment = require("moment");
var nodemailer = require("nodemailer");

const Client = require("authy-client").Client;
const authy = new Client({ key: process.env.API_KEY });


let login = (req, res) => { //login for email verify

  console.log(req.params);
  User.findOne({ email: req.params.email }).exec(function(err, user) {
    if (!user) { //if email isnt exist 
      return res.status(500).json({
        ok: false,
        err: "Email Not Found" //return email not found
      });
    } else if (user.emailverify === false) { // if emailverify is false 

      var id = user.id;
      console.log(req.params);
      let useremail = {
        emailverify: true // set true on emailverify
      };

      User.findByIdAndUpdate(// find user and update emailverify
        user._id,
        useremail,
        { new: true },
        (err, userNew) => {
          if (err) {
            return res.status(401).json({
              ok: false,// if error res false
              err
            });
          }else {
            createSession(req, res, user);// createSession funtion
          }
        }
      );
    } 
  });
};

function createSession(req, res, user) {//create session in ahuty
  req.session.regenerate(function() {
    req.session.loggedIn = true;
    req.session.user = user.id;
    req.session.email = user.email;
    req.session.msg = "Authenticated as: " + user.email;
    req.session.authy = false;
    req.session.ph_verified = false;
    let token = jwt.sign(//create jwt funtion
      {
        data: user
      },
      process.env.SECRET,
      { expiresIn: "10h" }
    );
    res.status(201).json({ // res session, token and user.
      ok: true,
      user: user,
      session: req.session,
      token
    });
  });
}

module.exports = {
  login
};
