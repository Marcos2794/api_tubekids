const User = require("../models/userModels");
const Client = require('authy-client').Client;
const authy = new Client({key: process.env.API_KEY});


let sms = (req, res) => { // send sms method
    console.log(req.body.email);
    User.findOne({email: req.body.email}).exec(function (err, user) {
        console.log("Send SMS");
        if (err) {
            console.log('SendSMS', err);
            res.status(500).json(err);
            return;
        }
        console.log(user);
        /**
         * If the user has the Authy app installed, it'll send a text
         * to open the Authy app to the TOTP token for this particular app.
         *
         * Passing force: true forces an SMS send.
         */
        authy.requestSms({authyId: user.authyId}, {force: true}, function (err, smsRes) {
            if (err) {
                console.log('ERROR requestSms', err);
                res.status(500).json({
                    ok:false,
                    err: err
                });
                return;
            }
            console.log("requestSMS response: ", smsRes);
            res.status(200).json(
                {
                    ok:true,
                    smsRes
                });
        });

    });
};



let verify = (req, res) => {
    User.findOne({email: req.body.email}).exec(function (err, user) {
        if (err) {
            console.error('Verify Token User Error: ', err);
            res.status(500).json(err);
        }
         /**
         * If the user has the Authy app installed, it'll verify a token
         *
         * verify token, with authyId and req.body.token.
         */
        authy.verifyToken({authyId: user.authyId, token: req.body.token}, function (err, tokenRes) {
            if (err) {
                console.log("Verify Token Error: ", err);
                res.status(500).json(err);
                return;
            }
            console.log("Verify Token Response: ", tokenRes);
            if (tokenRes.success) {
                req.session.authy = true;
            }
            res.status(200).json( {
                ok:true,
                tokenRes,
                authyId:  user.authyId
            });
        });
    });
};

// exports all methods
module.exports = {
    sms,
    verify
}