const Video = require("./../models/videoModels");


let create = (req, res) => {// create video

  if (//verify that all fields are full
    req.body.title.toString() === "" ||
    req.body.videotype.toString() === "" || 
    req.body.description.toString() === "" || 
    req.body.userid.toString() === "" ) {
    
    return res.status(500).json({
      ok: false,
      err: "verify that all fields are full"
    });
  }

//verify if video is file, if validation is true upload file
  if (req.body.videotype == "file") {
    console.log(req.body.videotype);
    let EDFile = req.files.url;
    console.log(EDFile);
    EDFile.mv(`./images/${EDFile.name}`, err => {
      if (err) {
        return res.status(401).json({
          ok: false,
          message: err
        });
      }
      console.log("FIle upload");
    });
  }

  let video = new Video({//create new video
    title: req.body.title,
    url: req.body.url,
    videotype: req.body.videotype,
    description: req.body.description,
    userid: req.body.userid
  });
  video.save((err, videoNew) => {// save new video
    if (err) {
      return res.status(401).json({
        ok: false,
        err
      });
    }
    return res.status(201).json({
      ok: true,
      video: videoNew
    });
  });
};

let list = (req, res) => {// list all videos with userid
  Video.find({ userid: req.params.userid }).exec((err, datos) => {
    return res.json(datos);
  });
};

let edit = (req, res) => {// return video for edit
  Video.find({ _id: req.params.id }).exec((err, datos) => {
    return res.json(datos);
  });
};

let update = (req, res) => {// update video
  let video = {
    title: req.body.title,
    url: req.body.url,
    videotype: req.body.videotype,
    description: req.body.description,
    userid: req.body.userid
  };

  Video.findByIdAndUpdate(// find video and update
    req.params.id,
    video,
    { new: true },
    (err, videoNew) => {
      if (err) {
        return res.status(401).json({
          ok: false,
          err
        });
      }
      return res.json({
        ok: true,
        videoNew
      });
    }
  );
};

let destroy = (req, res) => {

  // find video and delete
  Video.findByIdAndDelete(req.params.id, (err, video) => { 
    if (err) {
      return res.status(401).json({
        ok: false,
        err
      });
    }
    if (!video) {
      return res.status(404).json({
        ok: false,
        men: "The video dont exist"
      });
    }
    return res.json({
      ok: true,
      men: "Video was delete"
    });
  });
};

//exports all metods
module.exports = {
  list,
  create,
  update,
  destroy,
  edit
};
