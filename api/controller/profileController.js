const Profile = require("./../models/profileModels");

let logout = (req, res) => {// find session and destroy
  req.session.destroy(function(err) {
    if (err) {
      console.log("Error Logging Out: ", err);
      return next(err);
    }
    res.status(200).json({
      ok: true
    });
  });
};

let list = (req, res) => {// list all profiles with userid
  Profile.find({ adminid: req.params.userid }).exec((err, datos) => {
    return res.json(datos);
  });
};

let login = (req, res) => { // login for profile(childs)
  if (req.body.user.toString() === "" || req.body.pin.toString() === "") {// verify empty fields
    return res.status(500).json({
      ok: false,
      err: "verify that all fields are full"
    });
  }

  Profile.findOne({ user: req.body.user }).exec(function(err, user) {// find profile with user
    if (!user) {
      return res.status(500).json({
        ok: false,
        err: "User Not Found"
      });
    } else if ("pin" in req.body && user.pin !== parseInt(req.body.pin)) {// verify if pin isnt wrong
      console.log(req.body.pin);
      console.log(user.pin);
      return res.status(500).json({
        ok: false,
        err: "Wrong PIN"
      });
    } else {
      res.status(201).json({
        ok: true,
        profile: user
      });
    }
  });
};

let register = (req, res) => {// resgister profiles
  if (
    req.body.user.toString() === "" || 
    req.body.fullname.toString() === "" || 
    req.body.pin.toString() === "" || 
    req.body.adminid.toString() === "" ||
    req.body.age.toString() === ""  ) {
    return res.status(500).json({
      ok: false,
      err: "verify that all fields are full"
    });
  }

  var profile = req.body.user;

  Profile.findOne({ user: profile }).exec(function(err, user) {// verify if user exist
    if (err) {
      return res.status(500).json({
        ok: false,
        err: "bad conecction"
      });
    }
    if (user) {
      return res.status(409).json({// if user already registered res
        ok: false,
        err: "Username Already Registered"
      });
    }

    profile = new Profile({ user: req.body.user });// create a new childprofile

    console.log(req.body.adminid);
    profile.set("fullname", req.body.fullname);
    profile.set("pin", req.body.pin);
    profile.set("adminid", req.body.adminid);
    profile.set("age", req.body.age);
    profile.save(function(err) {
      if (err) {
        console.log("Error Creating User", err);
        res.status(500).json(err);
      } else {
        res.status(201).json({
          ok: true,
          user: user,
          adminid: req.body.adminid
        });
      }
    });
  });
};

let edit = (req, res) => {// find profile for edit
  Profile.find({ _id: req.params.id }).exec((err, datos) => {
    return res.json(datos);
  });
};

let update = (req, res) => {// method for update profile
  console.log(req.body);
  let profile = {
    fullname: req.body.fullname,
    user: req.body.user,
    pin: req.body.pin,
    age: req.body.age
  };

  Profile.findByIdAndUpdate(// edit profile, find with id
    req.params.id,
    profile,
    { new: true },
    (err, profileNew) => {
      if (err) {
        return res.status(401).json({// if update fail, res error
          ok: false,
          err
        });
      }

      return res.json({// if update succes, res new profile
        ok: true,
        profileNew
      });
    }
  );
};

let destroy = (req, res) => {

  // find profile and delete
  Profile.findByIdAndDelete(req.params.id, (err, profile) => { 
    if (err) {
      return res.status(401).json({
        ok: false,
        err
      });
    }
    if (!profile) {
      return res.status(404).json({
        ok: false,
        men: "The profile dont exist"
      });
    }
    return res.json({
      ok: true,
      men: "Profile was delete"
    });
  });
};

module.exports = {// expor all metods
  list,
  register,
  login,
  logout,
  edit,
  update,
  destroy
};
