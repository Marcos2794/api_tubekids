const User = require("./../models/userModels");
const bcrypt = require("bcrypt");
var crypto = require("crypto");
const jwt = require("jsonwebtoken");
var moment = require("moment");
var nodemailer = require("nodemailer");

const Client = require("authy-client").Client;
const authy = new Client({ key: process.env.API_KEY });

let logout = (req, res) => { // delete sessions 
  req.session.destroy(function(err) {
    if (err) {// if destroy sessions fail return err
      console.log("Error Logging Out: ", err);
      return next(err);
    }
    res.status(200).json({
      ok: true // if destroy sessions succes return ok: true
    });
  });
};

let index = (req, res) => {
  User.find({}).exec((err, datos) => {
    return res.json({
      datos
    });
  });
};

let login = (req, res) => {
  //verify that all fields are full
  if (req.body.email.toString() === "" || req.body.password.toString() === "") {
    return res.status(500).json({
      ok: false,
      err: "verify that all fields are full"
    });
  }

  User.findOne({ email: req.body.email }).exec(function(err, user) {//find the user whith email
    if (!user) {
      return res.status(500).json({//if email don exist return error
        ok: false,
        err: "Email Not Found"//if email don exist return error
      });
    } else if (//if password is wrong return error
      "password" in req.body &&
      user.hashed_password !== hashPW(req.body.password.toString())
    ) {
      return res.status(500).json({
        ok: false,
        err: "Wrong Password"//if password is wrong return error
      });
    } else if (user.emailverify === false) {
      return res.status(500).json({
        ok: false,
        err: "Dont verify email"//return error if email isnt verify 
      });
    } else {
      createSession(req, res, user);
    }
  });
};

let register = (req, res) => {
  var email = req.body.email;
  //verify that all fields are full
  if (
    req.body.email.toString() === "" ||
    req.body.password.toString() === "" ||
    req.body.name.toString() === "" ||
    req.body.lastname.toString() === "" ||
    req.body.country.toString() === "" ||
    req.body.birthdate.toString() === "" ||
    req.body.countrycode.toString() === "" ||
    req.body.phone.toString() === ""
  ) {
    return res.status(500).json({
      ok: false,
      err: "verify that all fields are full" //res err messaage if fileds are empty
    });
  }
  let age = moment(req.body.birthdate, "YYYYMMDD").fromNow(); //get user age

  if (parseInt(age) < 18) {
    // verify legal age
    return res.status(500).json({
      ok: false,
      err: "must be of legal age to register" //res err messaage if age are ilegal
    });
  }

  User.findOne({ email: email }).exec(function(err, user) {
    //find the user with your email
    if (err) {
      return res.status(500).json({
        //is db dont response res err
        ok: false,
        err: "Conection error"
      });
    }
    if (user) {
      //is user exist res Username Already Registered
      return res.status(409).json({
        ok: false,
        err: "Username Already Registered"
      });
    }

    console.log(req.body.password);

    user = new User({ email: req.body.email }); //create new user, set all params

    user.set("name", req.body.name);
    user.set("lastname", req.body.lastname);
    user.set("country", req.body.country);
    user.set("birthdate", req.body.birthdate);
    user.set("countrycode", req.body.countrycode);
    user.set("phone", req.body.phone);
    user.set("hashed_password", hashPW(req.body.password)); //save hash paswords in base64 with hasPW funtion
    user.set("authyId", null);
    user.set("emailverify", false);
    user.save(function(err) {
      if (err) {
        //if save user fail, res error
        console.log("Error Creating User", err);
        res.status(500).json(err);
      } else {
        //if save user succes, create a authy users
        authy.registerUser(
          //initialized funtion authy.registerUser
          {
            countryCode: req.body.countrycode.toString(),
            email: req.body.email,
            phone: req.body.phone
          },
          function(err, regRes) {
            if (err) {
              //if register in auhty fail, res error
              console.log("Error Registering User with Account Security");
              res.status(500).json(err);
              return;
            }

            user.set("authyId", regRes.user.id);
            console.log(req.body.phone, req.body.email, req.body.countrycode);
            // Save the AuthyID into the database then request an SMS
            user.save(function(err) {
              if (err) {
                //if register in auhty fail, res error
                console.log("error saving user in authyId registration ", err);
                res.session.error = err;
                res.status(500).json(err);
              } else {
                
                var transporter = nodemailer.createTransport({
                  //send email for verify
                  service: "Gmail",
                  auth: {
                    user: "tubekidsweb@gmail.com", // user that send email
                    pass: "secreto2019"
                  }
                });
                var mailOptions = {
                  // user that recive message
                  from: "Tubekids",
                  to: user.email,
                  subject: "Email verify",
                  text: "Welcome to tubekids",
                  html:
                    "<a href ='http://localhost:4200/emailverify/" +
                    user.email +
                    "'>aquí</a> para activar tu cuenta"
                };
                transporter.sendMail(mailOptions, function(error, info) {
                  // library method for send email
                  if (error) {
                    console.log(error);
                    res.status(500).json({
                      // if verification fail res error
                      ok: false,
                      err: "Email verify fail"
                    });
                  }
                });

                createSession(req, res, user); //if register succes, start funtion create session
              }
            });
          }
        );
      }
    });
  });
};

function hashPW(pwd) {
  console.log(pwd);
  return crypto
    .createHash("sha256")
    .update(pwd)
    .digest("base64")
    .toString();
}

function createSession(req, res, user) {
  //create session in ahuty

  req.session.regenerate(function() {
    req.session.loggedIn = true;
    req.session.user = user.id;
    req.session.email = user.email;
    req.session.msg = "Authenticated as: " + user.email;
    req.session.authy = false;
    req.session.ph_verified = false;
    let token = jwt.sign(
      //create jwt funtion
      {
        data: user
      },
      process.env.SECRET,
      { expiresIn: "10h" }
    );
    res.status(201).json({
      ok: true,
      user: user,
      session: req.session,
      token
    });
  });
}

module.exports = {
  index,
  register,
  login,
  logout
};
