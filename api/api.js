require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
var expressSession = require('express-session');
var mongoStore = require('connect-mongo')({session: expressSession});
var cookieParser = require('cookie-parser');

const app = express();

// default options
app.use(fileUpload());

//enable cors to allow restricted resources to be applied
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(
    expressSession(
        {//parse sessions
            'secret': process.env.SECRET,
            resave: true,
            saveUninitialized: true
        }
    )
);

/**
 * create a new mongo collections sessions
 */
mongoose.connect(process.env.MONGOURL, (err) => {
    if (err) {
        console.log(err);
    }
    app.use(expressSession({
        secret: process.env.SECRET,
        cookie: {maxAge: 60 * 60 * 1000},
        store: new mongoStore({
            db: mongoose.connection.db,
            collection: 'sessions'
        }),
        resave: true,
        saveUninitialized: true
    }));
    app.listen(process.env.PORT, function () {
        console.log('Example app listening on port ');
    });
});


app.get('/', function (req, res) {
    res.send('Welcome');
});

app.use(require("./routes/indexRoutes"));





