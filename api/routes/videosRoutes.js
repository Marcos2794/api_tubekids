const express = require('express');
const videoController = require('./../controller/videoController');

const { auth }  = require("./../middleware/autentication");


var router = express.Router();

/*
* This is the all routes to acces video  methods
*
* twofactorController verify if user is autenticate for enter the video views
*/
router.post("/video",auth, videoController.create);

router.get("/video/:userid", videoController.list);

router.get("/video/edit/:id", auth,videoController.edit);

router.put("/video/:id", auth, videoController.update);

router.delete("/video/:id", auth, videoController.destroy);

module.exports = router;
