
const express = require('express');
const profileController = require('./../controller/profileController');
const { auth }  = require("./../middleware/autentication");

var router = express.Router();

/*
* This is the all routes to acces profile  methods
*
* auth verify if user have administrator permission
*/
router.post("/register", auth,profileController.register);

router.post("/login", profileController.login);

router.get("/:userid", auth,profileController.list);

router.get("/edit/:id", auth,profileController.edit);

router.put("/:id", auth, profileController.update);

router.delete("/delete/:id", auth, profileController.destroy);

module.exports = router;