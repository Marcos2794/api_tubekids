const express = require('express');

var app = express();
/*
* in this part the routes are instantiated
*/

app.use("/api", require("./usersRoutes"));

app.use("/api", require("./videosRoutes"));

app.use("/api/profile", require("./profilesRoutes"));

module.exports = app;