const express = require('express');
const userController = require('./../controller/userController');
const emailverifyController = require('./../controller/emailverifyController');
const twofactorController = require('./../controller/twofactorController');

const  autentication   = require("./../middleware/autentication");

var router = express.Router();

/*
* This is the all routes to acces user  methods
*
* auth verify if user have administrator permission
*/
router.post("/register", userController.register);

router.post("/login", userController.login);

router.post("/emailverify/:email", emailverifyController.login);

router.post("/sms",twofactorController.sms);

router.post("/verify",twofactorController.verify);

router.post("/requireLoginAnd2FA",autentication.requireLoginAnd2FA);

router.get("/users",  userController.index);


module.exports = router;