const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let videoSchema = new Schema({
    title : String,
    url : String,
    videotype : String,
    description : String,
    userid : String
});

module.exports = mongoose.model("Video", videoSchema);