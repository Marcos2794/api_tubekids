const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var profileSchema = new Schema({
    fullname: {type: String, required: true},
    user: {type: String, required: true},
    pin: {type: Number, required: true},
    age: {type: Number, required: true},
    adminid: {type: String, required: true},
});



module.exports = mongoose.model('Profile', profileSchema);