const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {type: String, required: true},
    lastname: {type: String, required: true},
    countrycode: {type: Number, required: true},
    phone: {type: String, required: true},
    country: {type: String, required: true},
    birthdate: {type: Date, required: true},
    email: {type: String, required: true, unique: true},
   
    
    authyId: Number,
    emailverify: Boolean,
    hashed_password: String
});



module.exports = mongoose.model('User', userSchema);